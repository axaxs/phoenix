package main

import (
	"bufio"
	"bytes"
	"crypto/sha512"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"time"
	"unicode/utf8"
)

const (
	HOMEPATH string = "/opt/phoenix"
)

var (
	globholder = map[string]bool{}
	mt         sync.Mutex
)

type Message struct {
	Action    string `json:",omitempty"`
	Id        string `json:",omitempty"`
	Filename  string `json:",omitempty"`
	Location  string `json:",omitempty"`
	Data      string `json:",omitempty"`
	Result    string `json:",omitempty"`
	Tag       string `json:",omitempty"`
	Email     string `json:",omitempty"`
	Timestamp string `json:",omitempty"`
}

func timer(id string) {
	globholder[id] = true
	time.Sleep(10 * time.Minute)
	delete(globholder, id)
}

func startlogger() {
	f, err := os.OpenFile("phoenix.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Println("Could not open log file", err)
		os.Exit(1)
	}
	defer f.Close()
	log.SetOutput(f)
	c := make(chan os.Signal, 1)
	signal.Notify(c)
	<-c
	os.Exit(0)
}

func main() {
	go startlogger()
	certs, err := tls.LoadX509KeyPair("certs/phoenix.pem", "certs/phoenix.key")
	if err != nil {
		log.Fatalln(err)
	}
	conf := tls.Config{Certificates: []tls.Certificate{certs}}
	service := ":2683"
	listener, err := tls.Listen("tcp", service, &conf)
	if err != nil {
		log.Fatalln(err)
	}
	limchan := make(chan bool, 10)
	for i := 0; i < len(limchan); i++ {
		limchan <- false
	}
	for {
		<-limchan
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
		}
		log.Printf("Connection from: %s", conn.RemoteAddr().String())
		go workWith(conn, limchan)
	}
}

func workWith(conn net.Conn, lc chan bool) {
	defer conn.Close()
	defer func() {
		lc <- false
	}()
	tlscon, ok := conn.(*tls.Conn)
	if ok {
		retMsg := &Message{}
		err := tlscon.Handshake()
		if err != nil {
			log.Println(err)
			return
		}
		recv := make([]byte, 4096)
		var buf bytes.Buffer
		for {
			tlscon.SetReadDeadline(time.Now().Add(2 * time.Second))
			n, err := tlscon.Read(recv)
			if n > 0 {
				if recv[n-1] == byte(4) {
					buf.Write(recv[0 : n-1])
					break
				} else {
					buf.Write(recv[0:n])
				}
			}
			if err != nil {
				log.Println(err)
				break
			}
			if buf.Len() > 1000000 {
				retMsg.Result = "Fail - Message too large"
				tlscon.Write(buildResponse(retMsg))
				return
			}
		}
		jmsg, err := parseJSON(buf.Bytes())
		if err != nil {
			log.Println(err)
			retMsg.Result = "Fail - " + err.Error()
			tlscon.Write(buildResponse(retMsg))
			return
		}
		response := handleMessage(jmsg)
		tlscon.Write(append(response, byte(4)))
	}
}

func handleMessage(msg *Message) []byte {
	var resp []byte
	switch msg.Action {
	case "Register":
		resp = register(msg)
	case "Put":
		resp = put(msg)
	case "Pull":
		resp = pull(msg)
	case "List":
		resp = list(msg)
	case "Index":
		resp = index(msg)
	default:
		resp = []byte{}
	}
	return resp
}

func index(msg *Message) []byte {
	retMsg := &Message{}
	if msg.Email == "" {
		retMsg.Result = "Fail - Must Provide Email Address"
		return buildResponse(retMsg)
	}
	mt.Lock()
	defer mt.Unlock()
	f, err := os.Open(HOMEPATH + "/directory.index")
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - Error reading index"
		return buildResponse(retMsg)
	}
	defer f.Close()
	breader := bufio.NewReader(f)
	var allfound string
	x := 0
	for l, _, err := breader.ReadLine(); err == nil; l, _, err = breader.ReadLine() {
		slin := strings.Split(string(l), "|")
		if len(slin) > 2 {
			hasher := sha512.New()
			hasher.Write([]byte(slin[2] + msg.Email + strconv.Itoa(x)))
			emhash := hasher.Sum(nil)
			if string(emhash) == slin[2] {
				allfound += string(buildResponse(&Message{Tag: slin[1], Timestamp: slin[0]}))
			}
		}
		x++
	}
	if allfound != "" {
		retMsg.Result = "Success"
		retMsg.Tag = allfound
	} else {
		retMsg.Result = "Fail - No results found"
	}
	return buildResponse(retMsg)
}

func pull(msg *Message) []byte {
	retMsg := &Message{}
	if msg.Id == "" || msg.Tag == "" {
		retMsg.Result = "Fail - Must include at least Id and Tag"
		return buildResponse(retMsg)
	}
	data, err := ioutil.ReadFile(HOMEPATH + "/" + msg.Id + "/phoenix.index")
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - Could not read index file for this Id"
		return buildResponse(retMsg)
	}
	idx := strings.Split(string(data), "\n")
	for _, v := range idx {
		lin := strings.Split(v, "|")
		if len(lin) > 2 {
			if lin[0] == msg.Tag {
				retMsg.Tag = lin[0]
				retMsg.Filename = lin[2]
				retMsg.Location = lin[1]
			}
		}
	}
	if retMsg.Tag == "" {
		retMsg.Result = "Fail - Unknown file tag"
		return buildResponse(retMsg)
	}
	data, err = ioutil.ReadFile(HOMEPATH + "/" + msg.Id + "/" + msg.Tag)
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - Could not open file..."
		return buildResponse(retMsg)
	}
	retMsg.Data = base64.StdEncoding.EncodeToString(data)
	retMsg.Result = "Success"
	return buildResponse(retMsg)
}

func list(msg *Message) []byte {
	retMsg := &Message{}
	if msg.Id == "" {
		retMsg.Result = "Fail - Id Required"
		return buildResponse(retMsg)
	}
	data, err := ioutil.ReadFile(HOMEPATH + "/" + msg.Id + "/phoenix.index")
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - Could not read index file for this Id"
		return buildResponse(retMsg)
	}
	idx := strings.Split(string(data), "\n")
	taglist := ""
	for _, v := range idx {
		lin := strings.Split(v, "|")
		if len(lin) > 2 {
			sfile := &Message{}
			sfile.Tag = lin[0]
			sfile.Location = lin[1]
			sfile.Filename = lin[2]
			taglist += string(buildResponse(sfile))
		}

	}
	retMsg.Tag = taglist
	retMsg.Result = "Success"
	return buildResponse(retMsg)
}

func put(msg *Message) []byte {
	retMsg := &Message{}
	if msg.Id == "" || msg.Filename == "" || msg.Data == "" || msg.Location == "" {
		retMsg.Result = "Fail - Must include Id, Filename, Data, and Location"
		return buildResponse(retMsg)
	}
	_, ok := globholder[msg.Id]
	if !ok {
		retMsg.Result = "Fail - Id invalid or else expired"
		return buildResponse(retMsg)
	}
	data, err := base64.StdEncoding.DecodeString(msg.Data)
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - " + err.Error()
		return buildResponse(retMsg)
	}
	ok = utf8.Valid(data)
	if !ok {
		retMsg.Result = "Fail - Data being uploaded must be text!"
		return buildResponse(retMsg)
	}
	var filename string
	for {
		filename = randstring(12)
		_, err = os.Stat(HOMEPATH + "/" + msg.Id + "/" + filename)
		if err != nil {
			break
		}
	}

	err = ioutil.WriteFile(HOMEPATH+"/"+msg.Id+"/"+filename, data, 0744)
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - " + err.Error()
		return buildResponse(retMsg)
	}
	f, err := os.OpenFile(HOMEPATH+"/"+msg.Id+"/phoenix.index", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Println(err)
		retMsg.Result = "Fail - Error updating index"
		return buildResponse(retMsg)
	}
	defer f.Close()
	f.Write([]byte(filename + "|" + msg.Location + "|" + msg.Filename))
	retMsg.Result = "Success"
	return buildResponse(retMsg)
}

func randstring(length int) string {
	rand.Seed(time.Now().UnixNano())
	res := make([]byte, length)
	chars := []byte("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
	for i := 0; i < len(res); i++ {

		randno := rand.Intn(len(chars))
		ind := randno % (len(chars) - 1)
		res[i] = chars[ind]
	}
	return string(res)
}

func register(msg *Message) []byte {
	var dirname string
	retMsg := &Message{}
	for {
		dirname = randstring(5)
		err := os.Mkdir(HOMEPATH+"/"+dirname, 0755)
		if err != nil {
			log.Println(err)
			continue
		}
		break
	}
	if msg.Email != "" {
		mt.Lock()
		defer mt.Unlock()
		f, err := os.OpenFile(HOMEPATH+"/directory.index", os.O_RDWR|os.O_APPEND|os.O_CREATE, 0644)
		if err != nil {
			log.Println(err)
			retMsg.Result = "Fail - Error updating index"
			return buildResponse(retMsg)
		}
		defer f.Close()
		breader := bufio.NewReader(f)
		x := 0
		for _, _, err = breader.ReadLine(); err == nil; _, _, err = breader.ReadLine() {
			x++
		}
		hasher := sha512.New()
		hasher.Write([]byte(dirname + msg.Email + strconv.Itoa(x)))
		emhash := hasher.Sum(nil)
		tob := time.Now().Format("2006/01/02")
		f.Write([]byte(tob + "|" + dirname + "|" + string(emhash)))
	}
	go timer(dirname)
	return buildResponse(&Message{Id: dirname, Result: "Success"})
}

func buildResponse(res *Message) []byte {
	ret, err := json.Marshal(res)
	if err != nil {
		log.Println(err)
		return []byte(`{"Result":"Error"}`)
	}
	return ret
}

func parseJSON(inj []byte) (*Message, error) {
	msg := &Message{}
	err := json.Unmarshal(inj, msg)
	return msg, err
}
