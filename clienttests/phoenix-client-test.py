#!/usr/bin/python3

## This client registers, uploads file atestfile.file, lists, then downloads it as atestfile.file2

import socket
import ssl
import base64
import json

def main():
    y = connects()
    y.send('{"Action":"Register"}'.encode("utf8") + b'\x04')
    a = recv(y)
    print(a)
    j = json.loads(a)
    id = j["Id"]
    with open("atestfile.file", "rb") as f:
        d = f.read()
    z = base64.b64encode(d).decode("utf8")
    y = connects()
    y.send(('{"Data":"%s","Action":"Put","Location":"/home/user/config","Filename":"atestfile.file","Id":"%s"}' % (z,id)).encode("utf8") + b'\x04')
    a = recv(y)
    print(a)
    y = connects()
    y.send(('{"Action":"List","Id":"%s"}' % id).encode("utf8") + b'\x04')
    a = recv(y)
    j = json.loads(a)
    tags = j["Tag"]
    tags = tags.split(",")
    for t in tags:
        y = connects()
        y.send(('{"Action":"Pull","Id":"%s","Tag":"%s"}' % (id, j["Tag"])).encode("utf8") + b'\x04')
        a = recv(y)
        j = json.loads(a)
        with open(j["Filename"] + "2","wb") as f:
            f.write(base64.b64decode(j["Data"]))
        print(a)

def connects():
    x = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    x.connect(('phoenix.lx.lc', 2683))
    y = ssl.wrap_socket(x)
    return y

def recv(conn):
    tot = ""
    a = b'x'
    while 1:
        a = conn.recv(4096)
        if len(a) > 0:
            tot += a.decode("utf8")
            if a[-1] == 4:
                tot = tot[:-1]
                break
    return tot

main()
